/* Dependencies */
import React from 'react';

/* Style */
import './App.css';

/* Components */
import Header from './components/Header';
import Home from './components/Home';
import About from './components/About';
import Projects from './components/Projects';
import Contact from './components/Contact';
import Footer from './components/Footer';


function App() {
	return (
		<div className="App">
			<Header />
			<div className="Content">
				<Home />
				<Projects />
				<About />
				<Contact />
			</div>
			<Footer />
		</div>
	);
};

export default App;
