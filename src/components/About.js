import { useEffect } from 'react';
import './About.css';
import profilepic from '../assets/me.jpg'
import resume from '../assets/joakim-winther-resume.pdf'

function About() {

	useEffect(() => {
		document.querySelector('.Content').classList.add('load');
	})

	return (
		<div className="About" name="About">
			<div className="About-wrap">

			<h1 className="About-headline separator">More about me</h1>
				<div className="About-column">
					<p>Frontend Developer with a passion for technology and programming looking for a place to create. I have an extensive backpack of digital tools and an everything is possible atittude. A Digital Concept Development graduate, customer service professional and entrepreneur at heart in one package. 

                    </p><br />
                    <p>As a freelancer, I have successfully implemented websites from concept through deployment. I am no stranger to using different package managers, containers, CI/CD, new libraries or Git. Some technologies I have worked with lately include: </p>
					<ul style={{float:'left', width:'100%', marginLeft:'40px', marginTop:'10px', fontSize:'0.8em', marginBottom:'1em'}}>
						<li>JavaScript</li>
                        <li>React</li>
						<li>Node.js</li>
						<li>Python</li>
					</ul>

					<p>More about my experience can be found in my resume:</p>
					<a href={resume} target='_blank' rel='noopener noreferrer'><button className="button-cta">Resumé</button></a>
				</div>

				<div className="About-column">
					<img className="About-picture" src={profilepic} alt="me"/>
				</div>

			</div>
		</div>
	);
}

export default About;
