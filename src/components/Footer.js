import { AiFillGitlab } from 'react-icons/ai';
import { AiFillLinkedin } from 'react-icons/ai';

import Link from './Link';
import './Footer.css';

function Footer() {

  return (
    <footer className="Footer">
      <div className="Footer-wrap">

        <div className="Back-to-top">
          <Link to="Home" value="Back to top" /> 
        </div>
      
        <div className="Social-icons">
          <a href="https://www.linkedin.com/in/joakim-winther/"><AiFillLinkedin/></a>
          <a href="https://gitlab.com/joezcode/"><AiFillGitlab/></a>
        </div>
          
        <div className="by">Created by Joakim Winther</div>
        
      </div>
    </footer>
  );
}

export default Footer;
