import { useState } from "react";
// Style
import './Header.css';
// Components
import Navigation from './Navigation';
import Link from "./Link";

function Header() {
	// Check if page is scrolled, to add style on scroll
	const [isScrolled, setIsScrolled] = useState(false);

	const addHeaderShadow = () => {
	  let elementPosition = document.querySelector(".Home-message").getBoundingClientRect().top;
	  window.scrollY > elementPosition ? setIsScrolled(true) : setIsScrolled(false);
	}
  
	window.onscroll = () => {
	  addHeaderShadow();
	};
  
	const logo = (
		<span className="Logo">jw.code</span>
	);

	return (
		<div className={`Header ${isScrolled ? "Header-shadow" : ""}`}>
			<div className="Header-wrap">
				<Link to="Home" value={logo} />
				<Navigation />
			</div>
		</div>
		
	);
}

export default Header;
