import { useState } from "react";
import './Navigation.css';
import Link from "./Link";

function Navigation() {

	const [isOpen, setOpen] = useState(false);

	const pages = ['Home', 'Projects', 'About', 'Contact']

	const navLinks = pages.map(item => {
		return (
			<li className='Navigation-element' key={`Nav-el-${item.toLowerCase()}`}>
				<Link to={item} value={item} />
			</li>
		)
	});

	const mobileNavLinks = pages.map(item => {
		return (
			<li className='Mobile-nav-element' key={`Mob-el-${item.toLowerCase()}`}>
				<Link to={item} value={item} />
			</li>
		)
	});

	return (
	<>
		<nav className="Navigation">
			<ul>

			{navLinks}

			</ul>
		</nav>

		<nav className="Mobile-nav">
			<div onClick={() => setOpen(!isOpen)} className={`Hamburger ${isOpen ? "open" : "close"}`}>
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div className={`Hamburger-panel ${isOpen ? "open" : "close"}`}>
				<ul>

					{mobileNavLinks}

				</ul>
			</div>
		</nav>
	</>
	);
}

export default Navigation;
