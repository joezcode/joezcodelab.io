import './ProjectItem.css';
import { FiExternalLink } from 'react-icons/fi';
import { AiFillGitlab } from 'react-icons/ai';

function Project(props) {

	const imagePath = (filename) => {
		return require(`../assets/${filename}`)
	};

	const projectTitle = <h2 className="Project-title">{props.title ? props.title : 'Unnamed project'}</h2>;

	const projectLinks = (
		<div className="Project-links">
            {props.repository ? <a className="Project-repository" href={props.repository} target="_blank" rel="noreferrer"><AiFillGitlab /></a> : ''}
			{props.url ? <a className="Project-url" href={props.url} target="_blank" rel="noreferrer"><FiExternalLink/>{props.urlname && <em>{props.urlname}</em>}</a> : ''}
		</div>
	);

	const projectDescription = <p className="Project-description">{props.description ? props.description : 'Description missing.'}</p>;

	const projectTags = (
		<ul className="Project-tags">
			{props.tags.map(item => (
                <li className="Project-tag" key={`Project-tag-${item.toLowerCase()}`}>{item}</li>
			))}
		</ul>
	);

	return (
		<div className="Project">
			<div className="Project-image-wrap">
				<img className="Project-image" src={imagePath(props.image)} alt={props.title} />
			</div>
			<div className="Project-content">
				{projectTitle}
				{projectLinks}
				{projectDescription}
				{projectTags}
			</div>
		</div>
	)

}

export default Project;
