import './Projects.css';
import Project from './ProjectItem';
import data from '../assets/projectData.json';

function Projects() {


    const projects = data.map(item => {
        return (
            <Project 
                key={item.id}
                {...item}
            />
        )
    })

    return (       
        <div className="Projects" name="Projects">
            <h1 className="Project-headline separator">Selected projects</h1>
            <div className="Projects-wrap">
                {projects}
            </div>
      </div>
    )
}

export default Projects;
