import './ScrollDown.css';

function ScrollDown() {
    return (
        <div id="scroll-down">
                <span className="arrow-down">
                </span>
        </div>
    );
}

export default ScrollDown;
